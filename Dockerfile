FROM jupyter/base-notebook:lab-3.5.2

# download sem art
RUN mkdir ~/data && \
    wget -q -O ~/data/SemArt.zip https://researchdata.aston.ac.uk/id/eprint/380/1/SemArt.zip

RUN wget -q -O ~/data/art500k_toy.zip https://gohkust.sharepoint.com/:u:/t/DEEPART/EWV4TqtC2_hKliOC4RM18nAB4xfy83f1_EYEGpJDPaiDLA?download=1

RUN wget -q -O ~/data/art500k_toy_labels.csv https://deepart.hkust.edu.hk/ART500K/dataset/toy_dataset_label.csv

USER root

# gcc necessary for pymilvus...
RUN apt update && apt install -y unzip gcc g++

USER jovyan

# clean
RUN cd ~/data && unzip SemArt.zip && rm SemArt.zip

RUN cd ~/data && \
    unzip art500k_toy.zip -d ~/data/art500k_toy && \
	rm art500k_toy.zip && \
	mv art500k_toy_labels.csv ~/data/art500k_toy/toy_dataset_label.csv

# install other dependencies
RUN pip install -U jupyter-book
COPY --chown=${NB_UID}:${NB_GID} requirements.txt /tmp/
RUN pip install --quiet --no-cache-dir --requirement /tmp/requirements.txt
# idk why...
RUN pip install --quiet --upgrade jsonschema
